from django.shortcuts import render, redirect
from projects.models import Project
from django.urls import reverse

def overview(request):
    projects = Project.objects.all().order_by('-rank')
    context = {
        "projects": projects
    }
    return render(request, "projects/overview.html", context)

def detail(request, slug):
    project = Project.objects.get(slug=slug)
    context = {
        "project": project
    }
    return render(request, "projects/detail.html", context)

def handler500(request):
    url = reverse('project_index')
    return redirect(url, status=500)
