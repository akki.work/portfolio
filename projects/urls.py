from django.urls import path
from projects import views

urlpatterns = [
    path("", views.overview, name="project_index"),
    path("<slug:slug>/", views.detail, name="project_detail"),
]
