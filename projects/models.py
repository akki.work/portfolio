from django.db import models

class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    technology = models.CharField(max_length=100)
    image = models.FileField(blank=True)
    gitrepo = models.CharField(max_length=200, blank=True)
    livelink = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(default="", unique=True)
    rank = models.IntegerField(null=True)

    def __str__(self):
        return self.title